/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Class.java to edit this template
 */
package library;

/**
 *
 * @author jaiminlakhani
 */
public class GFG {
    public static void main(String[] args) {
        Book b1 = new Book("Effective Java", "Joshua Bloch");
        Book b2 = new Book("Thinking in java", "Bruce Eckel");
        Book b3 = new Book("Java: The Complete Reference", "Herbert Schildt");
        
        List <Book> books = new ArrayList<Book>;
        books.add(b1);
        books.add(b2);
        books.add(b3);
        
        Library library = new Library(Books);
        
        List<Book> bks = library.getTotalBooksInLibrary();
        for(book bk : bks) {
            System.out.println("Title :" + bk.title + " and " + " Author: " + bk.author);
        }
    }
}
