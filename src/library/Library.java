/*
 * Click nbfs://nbhost/SystemFileSystem/Templates/Licenses/license-default.txt to change this license
 * Click nbfs://nbhost/SystemFileSystem/Templates/Classes/Main.java to edit this template
 */
package library;

/**
 *
 * @author jaiminlakhani
 */
public class Library {
    private final List<Book> books;
    
    Library(List <Book> books) {
        this.books = books;
    }
    
    public List<Book> getTotalBooksInLibrary() {
        return books;
    }
}
